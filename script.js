console.log("an example site for freeCodeCamp challenge, using only bootstrap and jquery by FSBalbuena")
let indice=0
let quotes=[{text:"Read a thousand books, and your words will flow like a river.",
author:"Lisa See"}
,{text:"The first draft is just you telling yourself the story.",
author:"Terry Pratchett"}
,{text:"You can always edit a bad page. You can’t edit a blank page.",
author:"Jodi Picoult"}
,{text:"Start writing, no matter what. The water does not flow until the faucet is turned on.",
author:"Louis L’Amour"}
,{text:"Every secret of a writer’s soul, every experience of his life, every quality of his mind, is written large in his works.",
author:"Virginia Woolf"}
,{text:"And by the way, everything in life is writable about if you have the outgoing guts to do it, and the imagination to improvise. The worst enemy to creativity is self-doubt.",
author:"Sylvia Plath"}
,{text:"Don’t tell me the moon is shining; show me the glint of light on broken glass.",
author:"Anton Chekhov"}
,{text:"When your story is ready for rewrite, cut it to the bone. Get rid of every ounce of excess fat. This is going to hurt; revising a story down to the bare essentials is always a little like murdering children, but it must be done.",
author:"Stephen King"}
,{text:"There is no greater agony than bearing an untold story inside you.",
author:"Maya Angelou"}
,{text:"Don’t bend; don’t water it down; don’t try to make it logical; don’t edit your own soul according to the fashion. Rather, follow your most intense obsessions mercilessly.",
author:"Franz Kafka"}
,{text:"If the book is true, it will find an audience that is meant to read it.",
author:"Wally Lamb"}
,{text:"I can shake off everything as I write; my sorrows disappear, my courage is reborn.",
author:"Anne Frank"}
,{text:"People say, ‘What advice do you have for people who want to be writers?’ I say, they don’t really need advice, they know they want to be writers, and they’re gonna do it. Those people who know that they really want to do this and are cut out for it, they know it.",
author:"R.L. Stine"}
,{text:"I believe myself that a good writer doesn’t really need to be told anything except to keep at it.",
author:"Chinua Achebe"}]




function getRandomColor() {
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += '0123456789ABCDEF'[Math.floor(Math.random() * 16)];
  }
  return color;
}
function changeColors(color1,color2){
$("body").css("background",`linear-gradient(${color1},${color2})`)
 $("#tweet-quote").css("color",color1)
 $("#new-quote").css("backgroundColor",color1)
$("#text").css("color",color2)
}
function changeText(text,author){
  $("#text").text(`"${text}"`)
  $("#author").text(`-${author}`)
  $("#tweet-quote").attr("href",`https://twitter.com/intent/tweet/${text}-${author}`)
  
}
function newQuote(){
  indice=(indice==quotes.length-1)?0:indice+1
return quotes[indice]
}

let fetchNewQuote=function(){
changeColors(getRandomColor(),getRandomColor())
const {text,author}=newQuote()
changeText(text,author)
}

$("#new-quote").click(fetchNewQuote)
